<?php
    require_once __DIR__."./PwEncryption/Encrypter.php";
    require_once __DIR__."/../../vendor/autoload.php";

    /* $s = microtime(true); */
    $dotenv = \Dotenv\Dotenv::createImmutable(__DIR__."/../");
    $dotenv->safeLoad();

    //printf("%d: %3dms\n", 1,(microtime(true) - $s) * 1000)
    $encrypter = new Encrypter();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- Das neueste kompilierte und minimierte CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css">
    <!-- Optionales Theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <!-- Das neueste kompilierte und minimierte JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js"></script>
    <title>Testapi callsS</title>
</head>
<div class="container">
    <div class="py-5 text-center">
        <h2>Encrypt your password</h2>
        <p class="lead">Fill in the password field and encrypt your password</p>
    </div>
    <div class="row"> </div>
        <div class="col-md-8 order-md-1">
            <h4 class="mb-3">Fill in the password</h4>
            <form class="needs-validation" action="index.php" type="POST" role="form" novalidate>
                <div class="row">
                    <div class="mb-3">
                        <label for="pw">Password</label>
                        <input type="text" class="form-control" id="pw" name="pw" placeholder="" value="" required>
                        <div class="invalid-feedback">
                            Valid password is required.
                        </div>
                    </div>
                </div>
                <hr class="mb-4">
                <button class="btn btn-primary btn-lg btn-block" type="submit">Encrypt</button>
            </form>
        </div>
    </div>
    <?php if(isset($_GET['pw']) && strlen($_GET['pw']) > 0){ ?>
        <div style = "margin-left: 50px">
            <div class="row"> </div>
            <div class="col-md-8 order-md-1">
                <br /><br />
                <h4 class="mb-3">Password</h4>
                <form class="needs-validation" action="index.php" type="POST" role="form" novalidate>
                    <div class="row">
                        <div class="mb-3">
                            <div>
                                <strong>Encrypted Password: </strong><span><?php echo $encrypter->encryptLib($_GET['pw']); ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="mb-3">
                            <div>
                                <strong>Decrypted Password: </strong><span><?php echo $encrypter->decryptLib($encrypter->encryptLib($_GET['pw'])); ?></span>
                            </div>
                        </div>
                    </div>
                    <hr class="mb-4">
                </form>
            </div>
            </div>
        </div>
    <?php } ?>
    <footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1">&copy; 2021 SL</p>
    </footer>
</div>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../../assets/js/vendor/popper.min.js"></script>
<script src="../../dist/js/bootstrap.min.js"></script>
<script src="../../assets/js/vendor/holder.min.js"></script>
<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>
</body>
</html>