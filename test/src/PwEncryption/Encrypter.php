<?php

/**
 * Class Encrypter
 */
class Encrypter
{
    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $cipher;

    /**
     * @var string
     */
    private $tag;

    /**
     * @var \Defuse\Crypto\Key
     */
    private $keyForLib;

    /**
     * @var
     */
    private $testingKey;

    /**
     * @var
     */
    private $testingSavedKey;

    /**
     * @var
     */
    private $encryptionKey;

    /**
     * Encrypter constructor.
     */
    public function __construct(){
        //TODO: here load key from other sources
        $this->key        = "test";
        $this->cipher     = "aes-128-cbc";
        $this->key = \Defuse\Crypto\Key::loadFromAsciiSafeString(\Dotenv\Parser\EncrypterKey::KEY);
        //$this->tag      = "tag";
    }
    /**
     * Testing encryption without base64
     */
    public function encryptWoBase64($text){

        $bytes            = openssl_random_pseudo_bytes(openssl_cipher_iv_length($this->cipher));
        return bin2hex($bytes).":".openssl_encrypt($text, $this->cipher, $this->key, OPENSSL_RAW_DATA, $this->generateIV(bin2hex($bytes)));
    }
    /**
     * Testing decryption without base64
     */
    public function decryptWoBase64($text){
        $text       = $text;

        $data       = explode(":", $text);
        return (isset($data[0]) && isset($data[1])) ? openssl_decrypt($data[1], $this->cipher, $this->key, OPENSSL_RAW_DATA, $this->generateIV($data[0])) :null;
    }
    /**
     * Concatenate the iv with cipher
     * @param $text
     * @return false|string
     */
    public function encrypt($text){
        //Create random bytes 32 and the iv
        $bytes            = openssl_random_pseudo_bytes(openssl_cipher_iv_length($this->cipher));

        //return base64_encode(mcrypt_encrypt($this->cipher, $this->key, $text, "cbc", $this->generateIV(bin2hex($bytes))));
        return base64_encode(bin2hex($bytes).":".openssl_encrypt(strval($text), $this->cipher, $this->key, OPENSSL_RAW_DATA, $this->generateIV(bin2hex($bytes))));
        //return base64_encode(openssl_encrypt($text, $this->cipher, $this->key, $options=OPENSSL_RAW_DATA, $this->iv));
    }

    /**
     * Iv is concatenate with the cipher
     * @param $text
     * @return false|string
     */
    public function decrypt($text){
        $text       = base64_decode($text);
        //data[0] = iv, data[1] = cipher
        $data       = explode(":", $text);

        //return base64_encode(mcrypt_decrypt($this->cipher, $this->key, $data[1], "cbc", $this->generateIV(bin2hex($data[0]))));

        //return openssl_decrypt($plain, $this->cipher, $this->key, $options=OPENSSL_RAW_DATA, hex2bin($this->bytesClass));
        return (isset($data[0]) && isset($data[1])) ? openssl_decrypt($data[1], $this->cipher, \Defuse\Crypto\Key::loadFromAsciiSafeString(\Dotenv\Parser\EncrypterKey::KEY), OPENSSL_RAW_DATA, $this->generateIV($data[0])) :null;
    }

    public function encryptLib($text){
        return base64_encode(\Defuse\Crypto\Crypto::encrypt($text,  \Defuse\Crypto\Key::loadFromAsciiSafeString(\Dotenv\Parser\EncrypterKey::KEY), false));
    }

    public function decryptLib($text){
        return \Defuse\Crypto\Crypto::decrypt(base64_decode($text), \Defuse\Crypto\Key::loadFromAsciiSafeString(\Dotenv\Parser\EncrypterKey::KEY), false);
    }

    public function setDecryptionKey($key){
        $this->encryptionKey = \Defuse\Crypto\Key::loadFromAsciiSafeString($key);
    }

    private function generateIV($bytes){
        return hex2bin($bytes);
    }
}