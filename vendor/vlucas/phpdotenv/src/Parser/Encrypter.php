<?php
namespace Dotenv\Parser;

use Defuse\Crypto\Key;

/**
 * Class Encrypter
 */
class Encrypter
{
    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $cipher;

    /**
     * @var string
     */
    private $tag;

    /**
     * Encrypter constructor.
     */
    public function __construct(){
        //TODO: here load key from other sources
        $this->key      = \Defuse\Crypto\Key::loadFromAsciiSafeString(\Dotenv\Parser\EncrypterKey::KEY);
        $this->cipher   = "aes-128-cbc";
        $this->tag      = "tag";
        //$this->keyForLib = \Defuse\Crypto\Key::createNewRandomKey();
    }

    /**
     * Concatenate the iv with cipher
     * @param $text
     * @return false|string
     */
    public function encrypt($text){
        //Create random bytes 32 and the iv
        //$bytes            = openssl_random_pseudo_bytes(openssl_cipher_iv_length($this->cipher));

        //testingn here
        //return base64_encode(mcrypt_encrypt($this->cipher, $this->key, $text, $mode, $iv));
        //return base64_encode(bin2hex($bytes).":".openssl_encrypt($text, $this->cipher, $this->key, "cbc", $this->generateIV(bin2hex($bytes))));
        //return base64_encode(openssl_encrypt($text, $this->cipher, $this->key, $options=OPENSSL_RAW_DATA, $this->iv));
        return $this->encryptLib($text);
    }

    /**
     * @param $text
     * @return string
     */
    public function encryptLong($text){
        $bytes            = openssl_random_pseudo_bytes(openssl_cipher_iv_length($this->cipher));

        if(strlen($text) > 512){

        } else {
            $output = base64_encode(bin2hex($bytes).":".openssl_encrypt($text, $this->cipher, $this->key, OPENSSL_RAW_DATA, $this->generateIV(bin2hex($bytes))));
        }
        return $output;
    }

    /**
     * Iv is concatenate with the cipher
     * @param $text
     * @return false|string
     */
    public function decrypt($text){
        /*$text       = base64_decode($text);
        //data[0] = iv, data[1] = cipher

        $data       = explode(":", $text);
        //return openssl_decrypt($plain, $this->cipher, $this->key, $options=OPENSSL_RAW_DATA, hex2bin($this->bytesClass));
        return (isset($data[0]) && isset($data[1])) ? openssl_decrypt($data[1], $this->cipher, $this->key, OPENSSL_RAW_DATA, $this->generateIV($data[0])) :null;*/
        return $this->decryptLib($text);
    }

    public function encryptLib($text){
        return base64_encode(\Defuse\Crypto\Crypto::encrypt($text, $this->key, false));
    }

    public function decryptLib($text){
        $decrypt = \Defuse\Crypto\Crypto::decrypt(base64_decode($text), $this->key, false);
        return $decrypt;
    }

    /**
     * @param $bytes
     * @return false|string
     */
    private function generateIV($bytes){
        return hex2bin($bytes);
    }
}